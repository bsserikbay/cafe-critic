#!/bin/bash
REL_PATH=`dirname $0`
cd $REL_PATH
CURRENT_DIR=`pwd`

cd $CURRENT_DIR/../shop-api

NODE_ENV=test yarn run seed

cd $CURRENT_DIR/../shop-tests

npm start

exit 0
