const { I } = inject();
const execSync = require('child_process').execSync;

Before(()=> {
  execSync("NODE_ENV=test node ../backend/fixtures.js")
})

Given("Я нахожусь на странице {string}", (page) => {
  I.amOnPage(page);
});

When("я ввожу {string} в поле {string}", (text, fieldName) => {
  I.fillField({ css: fieldName }, text);
});

When("я нажимаю на кнопку {string}", (button) => {
  I.click(button);
});


Then("я вижу {string}", (text) => {
  I.see(text);
});
