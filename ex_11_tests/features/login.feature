# language: ru
Функция: Авторизация абонента.

  Сценарий: Успешная авторизация
    Допустим Я нахожусь на странице "/login"
    Если я ввожу "user" в поле "#username"
    И я ввожу "user" в поле "#password"
    И я нажимаю на кнопку "Вход"
    То я вижу "Привет, user"
