import React, {Component} from "react";
import FormElement from "../UI/Form/FormElement";
import Button from "@material-ui/core/Button";
import {Grid} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import Checkbox from '@material-ui/core/Checkbox';
import {connect} from "react-redux";
import {addNewPlace} from "../../store/actions/placeActions";
import AddImage from "../AddImage/AddImage";

class AddForm extends Component {
    state = {
        title: "",
        description: "",
        image: "",
    };


    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    onFileChangeHandler = e => {

        this.setState({image: e.target.files[0]}, ()=>
            console.log('image', this.state.image)
        );

    };

    onSubmitForm = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmitData(formData)

        console.log('form-data', formData)
    };


    render() {
        return (
            <Container>
                <h1>Добавить новое заведение</h1>

                <form onSubmit={this.onSubmitForm}>

                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <FormElement
                                type="text"
                                title="Название"
                                value={this.state.title}
                                name="title"
                                required={true}
                                placeholder="Название"
                                onChange={this.handleChange}
                                fieldName="title"
                                label="Название"
                            />
                        </Grid>
                        <Grid item xs={12}>

                            <FormElement
                                type="text"
                                title="Описание"
                                value={this.state.description}
                                name="description"
                                required={true}
                                placeholder="Описание"
                                onChange={this.handleChange}
                                fieldName="description"
                                label="Описание"
                            />

                        </Grid>

                        <Grid item xs={12}>
                            <AddImage
                                onFileChangeHandler={this.onFileChangeHandler}
                                onSubmitForm={this.onSubmitForm}
                            />

                            <p>
                                Прикрепите фотографию
                            </p>
                        </Grid>



                        <Grid item xs={6}>
                            <p>
                                Отправляя эту форму, вы соглашаетесь с тем, что следующая информация будет предоставлена
                                в общественное достояние, и администраторы этого сайта будут иметь полный контроль над
                                указанной информацией.
                            </p>
                        </Grid>

                        <Grid item xs={6}>
                            <Checkbox
                                color="primary"
                                inputProps={{'aria-label': 'secondary checkbox'}}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <Button onClick={this.onSubmitForm}
                                    variant="contained"
                                    color="primary"
                            >
                                Опубликовать
                            </Button>
                        </Grid>


                    </Grid>
                </form>
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSubmitData: formData => dispatch(addNewPlace(formData)),
    }
};

export default connect(null, mapDispatchToProps)(AddForm);
