import React from "react";
import FormElement from "../UI/Form/FormElement";
import Button from "@material-ui/core/Button";


function Comments(props) {


    return (

        <form action=""
              onSubmit={props.submit}
        >

            <FormElement
                title={"Комментарий"}
                label={"Комментарий"}
                fieldName={"comment"}
                onChange={props.changeComments}
                placeholder="Комментарий"
                value={props.comment}
                required={true}

            />

            <Button
                type={"submit"}
                variant="contained"
            >
                Отправить
            </Button>

        </form>
    )
}

export default Comments;