import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import Typography from "@material-ui/core/Typography";



export default function GetRating(props) {

    return (
        <div>
            <Box component="fieldset" borderColor="transparent">
                <div style={{display: "flex"}} >
                <Typography component="legend" style={{width: 150}}>Обслуживание</Typography>
                <Rating value={props.service} readOnly/>
                </div>

                <div style={{display: "flex"}}>
                <Typography component="legend" style={{width: 150}}>Еда</Typography>
                <Rating value={props.food} readOnly/>
                </div>

                <div style={{display: "flex"}}>
                <Typography component="legend" style={{width: 150}}>Интерьер</Typography>
                <Rating value={props.interior} readOnly/>
                </div>

            </Box>
        </div>
    );
}
