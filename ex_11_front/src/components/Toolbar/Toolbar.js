import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";
import {useHistory} from "react-router-dom";
import useStyles from "./useStyles";


export default function Header(props) {

    const classes = useStyles();
    const history = useHistory();
    const routeChange = () => {
        let path = `/`;
        history.push(path);
    }


    return (
        <div className={classes.root}>
            <AppBar position="relative">
                <Toolbar>

                    <div className={classes.logo}
                         onClick={routeChange}
                    >
                        <h1>Cafe critic</h1>
                    </div>

                    {props.user ?
                        <UserMenu
                            user={props.user}
                            logout={props.logout}
                        /> : <AnonymousMenu/>}

                </Toolbar>
            </AppBar>
        </div>
    );
}


