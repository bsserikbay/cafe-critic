import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
    logo: {
        flexGrow: 1,
        fontSize: 12,
        cursor: "pointer"
    },

}));

export default useStyles