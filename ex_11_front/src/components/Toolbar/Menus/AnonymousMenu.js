import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles((theme) => ({
  list: {
    display: "flex",
  },
  menuTitle: {
    fontSize: 16,
    [theme.breakpoints.down("sm")]: {
      fontSize: 14,
    },
  },
}));

const AnonymousMenu = () => {
  const classes = useStyles();

  const mouseEnterHandler = (e) => {
    e.target.style.color = "inherit";
  };

  const mouseLeaveHandler = (e) => {
    e.target.style.color = "inherit";
  };

  return (
    <div className={classes.list}>
      <MenuItem
        onMouseEnter={(e) => mouseEnterHandler(e)}
        onMouseLeave={(e) => mouseLeaveHandler(e)}
        component={Link}
        to="/register"
        className={classes.menuTitle}
      >
        Регистрация
      </MenuItem>

      <MenuItem
        onMouseEnter={(e) => mouseEnterHandler(e)}
        onMouseLeave={(e) => mouseLeaveHandler(e)}
        component={Link}
        to="/login"
        className={classes.menuTitle}
      >
        Войти
      </MenuItem>
    </div>
  );
};

export default AnonymousMenu;
