import React from "react";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import {Link} from "react-router-dom";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

export default function UserMenu(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const open = Boolean(anchorEl);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const userName = (arg) => {
        if (arg === "Admin" || arg === "admin") {
            return "Администратор";
        } else if (arg === "Inspector" || arg === "moderator") {
            return "Контролер";
        } else return arg;
    };

    return (
        <div>
            {userName(props.user.username)}
            <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
            >
                <AccountCircle/>
                <ArrowDropDownIcon/>
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                open={open}
                onClose={handleClose}
            >
                <MenuItem component={Link} to="/add" onClick={handleClose}>
                    Добавить заведение
                </MenuItem>

                <MenuItem onClick={props.logout}>Выход</MenuItem>
            </Menu>
        </div>
    );
}
