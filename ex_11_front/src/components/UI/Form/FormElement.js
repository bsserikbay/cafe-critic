import React from "react";
import PropTypes from "prop-types";
import FormGroup from '@material-ui/core/FormGroup';
import TextField from "@material-ui/core/TextField";

const FormElement = props => {
    return (
        <FormGroup>
            <TextField
                InputProps={props.class}
                helperText={props.error}
                variant="outlined"
                margin="normal"
                fullWidth
                label={props.label}
                title={props.title}
                type={props.type}
                name={props.fieldName}
                id={props.fieldName}
                placeholder={props.placeholder}
                value={props.value}
                onChange={props.onChange}
                required={props.required}
                InputLabelProps={{
                    shrink: true,
                }}
            />
        </FormGroup>
    );
};

FormElement.propTypes = {
    class: PropTypes.string,
    error: PropTypes.string,
    title: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    fieldName: PropTypes.string.isRequired,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool
}

export default FormElement;












