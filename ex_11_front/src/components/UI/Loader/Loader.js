import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    overlay: {
        position: "fixed",
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
        background: 'rgba(255, 255, 255, 0.6)'
    },
    loader: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        zIndex: 100000,
        marginTop: '-1.5rem',
        marginLeft: '-1.5rem'
    }
}));

const Loader = () => {
    const classes = useStyles();

    return (
        <div className={classes.overlay}>
            <CircularProgress color="primary" className={classes.loader}/>
        </div>
    );
};
export default Loader;
