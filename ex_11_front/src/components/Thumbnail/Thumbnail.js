import React, {Component} from "react";
import config from "../../config";
import defaultImage from "../../assets/images/default.jpg";
import {connect} from "react-redux";


class Thumbnail extends Component {

    state = {
        show: false
    }

    render() {
        let image = defaultImage;
        if (this.props.image) {
            image = config.apiURL + "/uploads/" + this.props.image;
        }


        return (
            <>

                <img src={image}
                     alt={this.props.title}
                     style={{
                         float: "left",
                         width: "100%",
                         height: "auto"
                     }}
                />
            </>
        )
    }
}

// const mapStateToProps = state => {
//     return {
//         fullPhoto: state.photos.fullPhoto,
//     };
// };

// const mapDispatchToProps = dispatch => {
    // return {
    //     onFetchFullView: (id) => dispatch(fetchFullView(id))
    // };
// };

export default connect(null, null)(Thumbnail);
