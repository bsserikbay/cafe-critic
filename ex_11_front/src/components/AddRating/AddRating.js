import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export default function AddRating(props) {

    return (
        <div>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <div style={{display: "flex"}}>
                <Typography component="legend" style={{width: 150}}>Обслуживание</Typography>
                <Rating
                    name="service"
                    value={props.serviceValue}
                    onChange={(event, newValue) => {
                        props.setServiceValue(newValue);
                    }}
                />
                </div>
                <div style={{display: "flex"}}>
                <Typography component="legend" style={{width: 150}}>Еда</Typography>
                <Rating
                    name="food"
                    value={props.foodValue}
                    onChange={(event, newValue) => {
                        props.setFoodValue(newValue);
                    }}
                />
                </div>

                <div style={{display: "flex"}}>
                <Typography component="legend" style={{width: 150}}>Интерьер</Typography>
                <Rating
                    name="interior"
                    value={props.interiorValue}
                    onChange={(event, newValue) => {
                        props.setInteriorValue(newValue);
                    }}
                />
                </div>
            </Box>


        </div>
    );
}
