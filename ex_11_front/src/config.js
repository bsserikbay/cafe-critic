let port = 8000;

if (process.env.REACT_APP_NODE_ENV === "test") {
  port = 8010;
}

let apiURL = "http://localhost";

export default {
  apiURL: `${apiURL}:${port}`
};

