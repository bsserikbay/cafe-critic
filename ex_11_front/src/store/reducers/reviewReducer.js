import {FETCH_COMMENT_SUCCESS,FETCH_COMMENT_REQUEST } from "../actionTypes";

const initialState = {
    comments: [],
    errors: "",
    loading: false,
};

const reviewReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENT_REQUEST:
            return {...state}

        case FETCH_COMMENT_SUCCESS:
            return {...state, comments: action.data}

        default:
            return state;
    }
};

export default reviewReducer;