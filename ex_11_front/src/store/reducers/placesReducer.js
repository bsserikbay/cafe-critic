import {CREATE_PLACE_SUCCESS, FETCH_PLACES_SUCCESS, FETCH_PLACE_SUCCESS} from "../actionTypes";

const initialState = {
    places: [],
    place: [],
    errors: "",
    loading: false,
};

const placesReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_PLACE_SUCCESS:
            return {...state}
        case FETCH_PLACES_SUCCESS:
            return {...state, places: action.cafes};
        case FETCH_PLACE_SUCCESS:
            return {...state, place: action.cafe}

        default:
            return state;
    }
};


export default placesReducer;
