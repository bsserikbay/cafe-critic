import {FETCH_IMAGE_SUCCESS} from "../actionTypes";

const initialState = {
    images: [],
    fullImage: '',
    errors: "",
    loading: false,
};

const imageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_IMAGE_SUCCESS:
            return {...state, images: action.images}

        default:
            return state;
    }
};

export default imageReducer;