import axios from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {FETCH_IMAGE_SUCCESS} from "../actionTypes";



const createImageSuccess = image => {
    return {type: FETCH_IMAGE_SUCCESS, image};
};

export const addNewPlaceImage = image => {
    if (image) {
        let formData = new FormData();
        console.log(formData.get("image"))
    return dispatch => {
        return axios.post("/images", image).then(() => {
            dispatch(createImageSuccess());
            NotificationManager.success("Фото опубликовано");
        });
    };
    }
    else {
        console.log("no file")
    }
};