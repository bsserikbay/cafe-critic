import axios from "../../axios-api";
// import {NotificationManager} from "react-notifications";
import {FETCH_COMMENT_SUCCESS, FETCH_COMMENT_REQUEST} from "../actionTypes";


const fetchCommentRequest = () => {
    return {type: FETCH_COMMENT_REQUEST};
};

const fetchCommentSuccess = data => {
    return {type: FETCH_COMMENT_SUCCESS, data};
};


export const sendReview = (comment, place, service, food, interior) => {

    const data = {comment, place, service, food, interior}
        return dispatch => {
            return axios.post("/reviews", data).then(() => {
                dispatch(fetchCommentRequest());
            });
        };
};

export const fetchReview = (id) => {

    return dispatch => {
        return axios.get(`/reviews?place=${id}`).then(response => {
            dispatch(fetchCommentSuccess(response.data));
        });
    };
};



