import axios from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {CREATE_PLACE_SUCCESS, FETCH_PLACES_SUCCESS, FETCH_PLACE_SUCCESS} from "../actionTypes";
import {push} from "connected-react-router";


const createPlaceSuccess = () => {
    return {type: CREATE_PLACE_SUCCESS};
};

const fetchPlacesSuccess = cafes => {
    return {type: FETCH_PLACES_SUCCESS, cafes};
};

const fetchPlaceSuccess = cafe => {
    return {type: FETCH_PLACE_SUCCESS, cafe};
}


export const addNewPlace = data => {
    return dispatch => {
        return axios.post("/places", data).then(() => {
            dispatch(createPlaceSuccess());
            dispatch(push("/"));
            NotificationManager.success("Кафе опубликовано");
        });
    };
};


export const fetchPlaces = (id) => {
    if (id) {
        return dispatch => {
            return axios.get(`/places/${id}`).then(response => {
                dispatch(fetchPlaceSuccess(response.data));
            });
        };
    } else {
        return dispatch => {
            return axios.get("/places").then(response => {
                dispatch(fetchPlacesSuccess(response.data));
            });
        };
    }


};
