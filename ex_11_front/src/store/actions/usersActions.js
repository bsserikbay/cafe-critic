import axios from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";
import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS,
} from "../actionTypes";

const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS};
};
const registerUserFailure = (error) => {
    return {type: REGISTER_USER_FAILURE, error};
};

const loginUserSuccess = (user) => {
    return {type: LOGIN_USER_SUCCESS, user};
};

export const registerUser = (userData) => {
    return (dispatch) => {
        return axios.post("/users", userData).then(
            (response) => {
                dispatch(registerUserSuccess());
                dispatch(loginUserSuccess(response.data));
                if (response.data.role === "admin") {
                    dispatch(push("/"));
                } else {
                    dispatch(push("/"));
                }
                NotificationManager.success("Регистрация прошла успешно!");
            },
            (error) => {
                if (error.response.data) {
                    dispatch(push("/"));
                    NotificationManager.warning(error.response.data.message);

                } else if (
                    error.response &&
                    error.response.data &&
                    error.response.data.message
                ) {

                    dispatch(push("/"));
                    NotificationManager.warning(error.response.data.message);
                } else {
                    dispatch(registerUserFailure({global: "Нет интернет соединения"}));
                }
            }
        );
    };
};

const loginUserFailure = (error) => {
    return {type: LOGIN_USER_FAILURE, error};
};
export const loginUser = (userData) => {
    return (dispatch) => {
        axios.post("/users/sessions", userData).then(
            (response) => {
                dispatch(loginUserSuccess(response.data));
                if (response.data.role === "admin") {
                    dispatch(push("/"));
                } else if (response.data.role === "user") {
                    dispatch(push("/"));
                } else {
                    dispatch(push("/"));
                }
                NotificationManager.success("Привет, " + response.data.username);
            },
            (error) => {
                NotificationManager.warning(error.response.data.error);
                error.response.data.message &&
                dispatch(loginUserFailure(error.response.data.message));
            }
        );
    };
};

export const logoutUser = () => {
    return (dispatch) => {
        return axios.delete("/users/sessions").then(() => {
                dispatch({type: LOGOUT_USER});
                dispatch(push("/"));
                NotificationManager.warning("До свидания!");
            },
            (error) => {
                NotificationManager.warning(error.response.data.error);
            }
        );
    };
};
