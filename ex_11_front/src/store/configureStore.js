import { connectRouter, routerMiddleware } from "connected-react-router";
import thunkMiddleware from "redux-thunk";
import { createBrowserHistory } from "history";
import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import { loadFromLocalStorage, saveToLocalStorage } from "./localStorage";
import usersReducer from "../store/reducers/usersReducer";
import placesReducer from "./reducers/placesReducer";
import imageReducer from "./reducers/imageReducer"
import reviewReducer from "./reducers/reviewReducer";

export const history = createBrowserHistory();


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  users: usersReducer,
  places: placesReducer,
  images: imageReducer,
  review: reviewReducer,
  router: connectRouter(history),
});

const middleware = [thunkMiddleware, routerMiddleware(history)];

const enhancers = composeEnhancers(applyMiddleware(...middleware));


const defaultState = loadFromLocalStorage();

const store = createStore(rootReducer, defaultState, enhancers);

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      user: store.getState().users.user,
    },
  });
});

export default store;
