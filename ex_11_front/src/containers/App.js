import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import {logoutUser} from "../store/actions/usersActions";
import Toolbar from "../components/Toolbar/Toolbar";
import { NotificationContainer } from "react-notifications";
import Routes from "../Routes";

class App extends Component {
  render() {
    return (
        <>
          <NotificationContainer />
          <Toolbar
              user={this.props.user}
              logout={this.props.logoutUser}
          />
          <main>
            <Routes user={this.props.user}/>
          </main>
        </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.users.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
