import React, {useState} from "react";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";
import FormGroup from "@material-ui/core/FormGroup";
import {
    Avatar,
    Button,
    Container,
    CssBaseline,
    Typography,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import useStyles from "./useStyles";
import Alert from "@material-ui/lab/Alert";


function Register(props) {
    const classes = useStyles();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const usernameInputHandler = (e) => {
        setUsername(e.target.value);
    };

    const passwordChangeHandler = (e) => {
        setPassword(e.target.value);
    };

    const submitFormHandler = (e) => {
        e.preventDefault();
        props.onUserRegistered({username, password});
    };

    const getFieldError = fieldName => {
        return props.error && props.error.errors &&
            props.error.errors[fieldName] &&
            props.error.errors[fieldName].properties &&
            props.error.errors[fieldName].properties.message;
    };

    return (
        <>
            {props.error &&<Alert color="warning">{props.error}</Alert>}

            <Container maxWidth="lg" className={classes.container}>
                <CssBaseline/>
                <div className={classes.wrapper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h6" className={classes.title}>
                        Регистрация нового пользователя
                    </Typography>
                    <form onSubmit={submitFormHandler} className={classes.form}>
                        <FormElement
                            type="text"
                            title="Имя"
                            name="username"
                            error={getFieldError("username")}
                            label=""
                            fieldName="username"
                            required={false}
                            placeholder="Имя пользователя"
                            helperText=""
                            value={username}
                            onChange={usernameInputHandler}
                            class={{
                                classes: {input: classes.input},
                            }}
                        />
                        <FormElement
                            type="password"
                            title="Пароль"
                            name="password"
                            fieldName="password"
                            error={getFieldError("password")}
                            helperText=""
                            required={false}
                            placeholder="Создайте пароль"
                            label=""
                            value={password}
                            onChange={passwordChangeHandler}
                            class={{
                                classes: {input: classes.input},
                            }}
                        />

                        <FormGroup row>
                            <Button
                                fullWidth={true}
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Создать
                            </Button>
                        </FormGroup>
                    </form>
                </div>
            </Container>
        </>
    );
}

const mapStateToProps = (state) => ({
    error: state.users.registerError,
});

const mapDispatchToProps = (dispatch) => ({
    onUserRegistered: (userData) => dispatch(registerUser(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
