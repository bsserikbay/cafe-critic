import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    container: {
        padding: "7% 0",
    },
    wrapper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        backgroundColor: "#3f51b5",
    },
    form: {
        width: "35%",
        marginBottom: 20,
    },

    input: {
        "&::placeholder": {
            fontSize: 16,
            [theme.breakpoints.down("sm")]: {
                fontSize: 13,
            },
        },
    },
    title: {
        [theme.breakpoints.down("sm")]: {
            fontSize: 16,
        },
    },
}));

export default useStyles