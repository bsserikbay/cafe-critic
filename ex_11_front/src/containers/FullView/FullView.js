import React, {useEffect, useState} from "react";
import {fetchPlaces} from "../../store/actions/placeActions";
import {connect} from "react-redux";
import {Container} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import {makeStyles} from "@material-ui/core/styles";
import Comments from "../../components/Comments/Comments";
import {sendReview, fetchReview} from "../../store/actions/reviewActions";
import AddRating from "../../components/AddRating/AddRating";
import GetRating from "../../components/GetRating/GetRating";

const useStyles = makeStyles({
    root: {
        padding: 20
    },
    title: {
        fontSize: 22,
        color: "black"
    },
    border: {
        border: "1px solid grey",
        borderRadius: 4,
        padding: 20,
        margin: "10px 0",
        cursor: "pointer"
    },
    li: {
        border: "1px solid grey",
        borderRadius: 4,
        width: "100%",
        margin: 10
    }
});

function FullView(props) {
    const classes = useStyles();

    const [comment, setComment] = useState("");
    const [service, setService] = useState(0);
    const [food, setFood] = useState(0);
    const [interior, setInterior] = useState(0)

    useEffect(() => {
        props.onFetchCafes(props.match.params.id);
        props.fetchReview(props.match.params.id);
    }, [comment]) // eslint-disable-line react-hooks/exhaustive-deps

    const changeComments = (e) => {
        setComment(e.target.value)
    };

    const submitComment = (e) => {
        e.preventDefault()
        props.onSendReview(comment, props.match.params.id, service, food, interior).then(() => {
            props.fetchReview(props.match.params.id);
        }).then(() => {
            setComment("");
        });
        setComment("");
        setService(0);
        setFood(0)
        setInterior(0)
    };

    const x = props.place.image

    return (
        <Container maxWidth="lg" className={classes.root}>

            <Grid container>
                <Grid item xs={12}>
                    <h1>{props.place.title}</h1>
                    <p>{props.place.description}</p>
                    <>
                        {
                            Object.keys(x).map(i => {
                                return <Thumbnail
                                key={x[i]._id}
                                image={x[i].photo}
                                />
                            })
                        }
                    </>
                </Grid>
            </Grid>


            <Grid container maxWidth="lg" className={classes.root}>
                <ul style={{listStyleType: "none", width: "100%"}}>
                    {
                        props.review.map(item => {
                            return <li key={item._id} style={{padding: 5}} className={classes.li}>
                                <p style={{padding: 20, fontWeight: 700}}>
                                    {item.user.username} wrote: {item.comment}
                                </p>

                                <GetRating
                                    food={item.food}
                                    service={item.service}
                                    interior={item.interior}
                                />

                            </li>
                        })
                    }
                </ul>

            </Grid>

            <Grid container maxWidth="lg" className={classes.root}>
                <Grid item xs={12} className={classes.border}>
                    <AddRating
                        foodValue={food}
                        serviceValue={service}
                        interiorValue={interior}
                        setFoodValue={(newValue) => setFood(newValue)}
                        setInteriorValue={(newValue) => setInterior(newValue)}
                        setServiceValue={(newValue) => setService(newValue)}
                    />
                </Grid>

                <Grid item xs={12} className={classes.border}>
                    <Comments
                        changeComments={changeComments}
                        comments={comment}
                        submit={(e) => submitComment(e)}
                        fullWidth={true}

                    />
                </Grid>

            </Grid>
        </Container>
    )
}

const mapStateToProps = state => ({
    place: state.places.place,
    user: state.users.user,
    review: state.review.comments
});

const mapDispatchToProps = dispatch => ({
    onFetchCafes: (id) => dispatch(fetchPlaces(id)),
    onSendReview: (comment, place, service, food, interior) => dispatch(sendReview(comment, place, service, food, interior)),
    fetchReview: (id) => dispatch(fetchReview(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(FullView)
