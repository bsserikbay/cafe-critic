import React, {useState} from "react";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {loginUser} from "../../store/actions/usersActions";
import Alert from "@material-ui/lab/Alert";
import {
    Avatar,
    Button,
    Container,
    CssBaseline,
    FormGroup,
    Typography,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {makeStyles} from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
    container: {
        padding: "7% 0",
    },
    wrapper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        backgroundColor: "#3f51b5",
    },
    form: {
        width: "35%",
        marginBottom: 20,
    },

    input: {
        "&::placeholder": {
            fontSize: 16,
            [theme.breakpoints.down("sm")]: {
                fontSize: 13,
            },
        },
    },
    title: {
        [theme.breakpoints.down("sm")]: {
            fontSize: 16,
        },
    },
}));

function Login(props) {
    const classes = useStyles();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const usernameInputHandler = (e) => {
        setUsername(e.target.value);
    };

    const passwordChangeHandler = (e) => {
        setPassword(e.target.value);
    };

    const submitFormHandler = (e) => {
        e.preventDefault();
        props.loginUser({username, password});
    };

   const getFieldError = fieldName => {
        return props.error && props.error.errors &&
            props.error.errors[fieldName] &&
            props.error.errors[fieldName].properties &&
            props.error.errors[fieldName].properties.message;
    };

    return (
        <>
            {props.error && <Alert color="warning">{getFieldError(props.error)}</Alert>}

            <Container maxWidth="lg" className={classes.container}>
                <CssBaseline/>

                <div className={classes.wrapper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h6" className={classes.title}>
                        Вход в систему
                    </Typography>

                    <form onSubmit={submitFormHandler} className={classes.form}>
                        <FormElement
                            type="text"
                            title="Имя"
                            label=""
                            fieldName="username"
                            error={getFieldError("username")}
                            name="username"
                            required={false}
                            placeholder="Имя пользователя"
                            value={username}
                            onChange={usernameInputHandler}
                            helperText=""
                            class={{
                                classes: {input: classes.input},
                            }}
                        />
                        <FormElement
                            type="password"
                            title="Пароль"
                            fieldName="password"
                            error={getFieldError("password")}
                            name="password"
                            required={false}
                            label=""
                            placeholder="Пароль"
                            value={password}
                            onChange={passwordChangeHandler}
                            helperText=""
                            class={{
                                classes: {input: classes.input},
                            }}
                        />

                        <FormGroup row>
                            <Button
                                fullWidth={true}
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Вход
                            </Button>
                        </FormGroup>
                    </form>
                </div>
            </Container>
        </>
    );
}

const mapStateToProps = (state) => ({
    error: state.users.loginError,
});


const mapDispatchToProps = (dispatch) => ({
    loginUser: (userData) => dispatch(loginUser(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
