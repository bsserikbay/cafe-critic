import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {fetchPlaces} from "../../store/actions/placeActions";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    root: {
        padding: 20
    },
    title: {
        fontSize: 22,
        color: "black"
    },
    border: {
        border: "1px solid grey",
        borderRadius: 4,
        margin: 20,
        cursor: "pointer"
    },
});

function MainPage(props) {
    const classes = useStyles();

    useEffect(() => {
        props.onFetchCafes()
    },[props.places.length]) // eslint-disable-line react-hooks/exhaustive-deps

    console.log(props.places)

        return (
            <Container maxWidth="lg" className={classes.root}>
                <Grid container  spacing={3}>
                {props.places.map((place) => {
                    return (
                        <Grid item
                              key={place._id}
                              xs={3}
                              className={classes.border}
                              component={Link}
                              to={`/places/${place._id}`}
                        >
                            <h1 className={classes.title}>{place.title}</h1>
                            {
                                place.image.map((item) => {
                                    return (
                                        <div key={item._id}>
                                            <div>
                                                <Thumbnail
                                                    image={item.photo}
                                                />
                                            </div>

                                        </div>
                                    )
                                })
                            }
                        </Grid>
                    )
                })}
                </Grid>
            </Container>
        );
}

const mapStateToProps = state => ({
        places: state.places.places,
        user: state.users.user
});

const mapDispatchToProps = dispatch => ({
        onFetchCafes: () => dispatch(fetchPlaces())
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage)

