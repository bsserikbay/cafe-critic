import React from "react";
import { Route, Switch } from "react-router-dom";
import { Redirect } from "react-router";
import MainPage from "./containers/MainPage/MainPage";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddNew from "./containers/AddNew/AddNew";
import AddImage from "./components/AddImage/AddImage"
import FullView from "./containers/FullView/FullView";

const ProtectedRoute = (props) => {
  return props.isAllowed ? (
    <Route {...props} />
  ) : (
    <Redirect to={props.redirectTo} />
  );
};

const Routes = ({ user }) => {
  return (
    <Switch>
      <Route path="/" component={MainPage} exact />

      <ProtectedRoute
        path="/register"
        component={Register}
        exact
        isAllowed={!user}
        redirectTo="/"
      />
      <ProtectedRoute
        path="/login"
        component={Login}
        exact
        isAllowed={!user}
        redirectTo="/"
      />
        <ProtectedRoute
            path="/places"
            component={MainPage}
            exact
            isAllowed={!user}
            redirectTo="/"
        />



        <ProtectedRoute
            path="/add"
            component={AddNew}
            exact
            isAllowed={user}
            redirectTo="/"
        />

        <ProtectedRoute
            path="/image"
            component={AddImage}
            exact
            isAllowed={user}
            redirectTo="/"
        />


        <ProtectedRoute
            path="/places/:id"
            component={FullView}
            exact
            isAllowed={user}
            redirectTo="/"
        />



      <Route
        render={() => {
          return (
            <>
              <h1 style={{ textAlign: "center" }}>
                Not found <br /> <small>404</small>
              </h1>
            </>
          );
        }}
      />
    </Switch>
  );
};
export default Routes;
