const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PlaceSchema = new Schema({

    title: {
        type: String,
        // required: true
    },

    description: {
        type: String,
        // required: true
    },

    review: {
        type: Schema.Types.ObjectId,
        ref: "Review",
        // required: true
    },
    image:[{
        type: Schema.Types.ObjectId,
        ref: 'Image',
        // required: true
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    }
});

const Place = mongoose.model("Place", PlaceSchema);
module.exports = Place;
