const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ImageSchema = new Schema({
    place: {
        type: Schema.Types.ObjectId,
        ref: "Place",
        required: true
    },
    photo: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },

});

const Image = mongoose.model("Image", ImageSchema);
module.exports = Image;
