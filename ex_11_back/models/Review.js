const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    place: {
        type: Schema.Types.ObjectId,
        ref: "Place",
        required: true
    },

    comment: {
        type: String,
        required: true
    },

    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    food: {
        type: Number
    },
    service: {
        type: Number
    },
    interior: {
        type: Number
    }
});

const Review = mongoose.model("Review", ReviewSchema);
module.exports = Review;
