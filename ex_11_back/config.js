const path = require("path");

const rootPath = __dirname;

let dbName = "exam_12"
if (process.env.NODE_ENV === "test"){
  dbName = "exam_test"
}

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public", "uploads"),
  db: {
    name: dbName,
    url: "mongodb://localhost"
  },
  facebook: {
    appId: "3343282342401995",
    secret: "cd67d8c7033f44078d95e180999a5b99"
  }
};
