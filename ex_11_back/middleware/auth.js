const User = require("../models/User");

const auth = async (req, res, next) => {
  const token = req.get("Token");
  if (!token) {
    return res.status(401).send({error: "Token was not presented"});
  }
  const user = await User.findOne({token});
  if (!user) {
    return res.status(401).send({error: "You must be logged in!"});
  }

  req.user = user;
  next();
};

module.exports = auth;