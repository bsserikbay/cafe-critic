const Review = require('../models/Review');
const auth = require("../middleware/auth");
const express = require("express");
const router = express.Router();
const User = require("../models/User")


const createRouter = () => {

    router.get('/', async (req, res) => {

            if (req.query.place) {
                let place = req.query.place;
                await Review.find({place})
                    .populate("user")
                    .exec((err, review) => {
                        res.send(review)
                    })
            } else {
                const review = await Review.find().populate("place")
                res.send(review)
            }




        }
    );


    router.post('/', auth, async (req, res) => {
        const token = req.get("Token");
        const user = await User.findOne({token})
        const userId = user._id;
        try {
            const review = await new Review(req.body);
            review.user = userId;
            review.place = req.body.place;
            review.comment = req.body.comment;
            review.service = req.body.service;
            review.interior = req.body.interior;
            review.food = req.body.food;
            review.save()
            res.send({message: "We posted"})

        } catch (error) {
            res.send(error);
        }

    });
    return router;
}


module.exports = createRouter;

