const express = require("express");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Image = require("../models/Image");
const User = require("../models/User")
const auth = require("../middleware/auth");
const Place = require("../models/Place");
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/", async (req, res) => {
        try {
            await Place.find().populate("image").populate("review")
                .exec((err, places) => {
                    res.send(places)
                    console.log('places', places)
                })

        } catch (e) {
            res.sendStatus(500);
        }
    });

    router.get("/:id", async (req, res) => {
        const place = await Place.findById(req.params.id).populate("image");
        res.send(place);
    });


    router.post("/", [upload.single("image"), auth], async (req, res) => {

            const token = req.get("Token");
            const user = await User.findOne({token})
            const userId = user._id
            const place = new Place(req.body)
            place.title = req.body.title
            place.description = req.body.description
            place.user = userId
            const image = new Image(req.body);

            if (req.file) {
                image.photo = req.file.filename;
                image.user = userId
                image.place = place._id
                await image.save().then(() => {
                    place.image.push(image)
                })

            }
            if (!req.file) {
                console.log('no image found')
            }

            await place.save()
            res.send(place);
        }
    );

    router.delete("/:id", auth, async (req, res) => {
        const token = req.get("Token");
        const user = await User.findOne({token})
        const id = req.params.id
        try {
            await Image.deleteOne({_id: id, user})
            res.sendStatus(200)
        } catch (e) {
            res.sendStatus(e)
        }
    });

    return router;
}


module.exports = createRouter;
