const express = require("express");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Image = require("../models/Image");
const User = require("../models/User")
const auth = require("../middleware/auth");
const Place = require("../models/Place");
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {


    router.post("/", [upload.single("image"), auth], async (req, res) => {

            const token = req.get("Token");
            const user = await User.findOne({token})
            const userId = user._id
            const image = new Image(req.body);
            try {
                if (req.file) {
                    console.log('file', req.file)
                    image.photo = req.file.filename;
                    image.user = userId
                    // image.place = place._id
                    await image.save()
                }

                if (!req.file) {
                    console.log('no image found')
                }

            } catch (e) {
                console.log(e)
            }

            res.send(image);
        }
    )
    ;

    // router.delete("/:id", auth, async (req, res) => {
    //     const token = req.get("Token");
    //     const user = await User.findOne({token})
    //     const id = req.params.id
    //     try {
    //         await Image.deleteOne({_id: id, user})
    //         res.sendStatus(200)
    //     } catch (e) {
    //         res.sendStatus(e)
    //     }
    // });

    return router;
}


module.exports = createRouter;
