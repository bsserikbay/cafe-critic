const express = require("express");
const axios = require("axios");
const {nanoid} = require("nanoid");
const config = require("../config");
const User = require("../models/User");

const createRouter = () => {
  const router = express.Router();

  router.get("/", async (req, res) => {
    try {
      res.send(await User.find());
    } catch (e) {res.status(500).send(e)}
  });

  router.post("/", async (req, res) => {
    const user = new User({
      username: req.body.username,
      password: req.body.password
    });

    try {

      await user.save();
      res.send(user);
    } catch (e) {res.status(400).send(e)}


  });

  router.post("/sessions", async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(400).send({error: "Username not found"});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(400).send({error: "Wrong password"});
    }

    user.generateToken();

    await user.save();

    res.send(user);

  });

  router.delete("/sessions", async (req, res) => {
    const token = req.get("Token");
    const success = {message: "Success"};

    if (!token) return res.send(success);

    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    await user.save()

    res.send(success);

  });

  router.post("/facebookLogin", async (req, res) => {
    console.log(req.body)
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + "|" + config.facebook.secret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
    try {
      const response = await axios.get(debugTokenUrl);

      if (response.data.data.error) {
        return res.status(401).send({message: "Facebook token incorrect"});
      }
      if (req.body.id !== response.data.data.user_id) {
        return res.status(401).send({message: "Wrong user ID"});
      }
      let user = await User.findOne({facebookId: req.body.id});

      if (!user) {
        user = new User({
          username: req.body.email,
          password: nanoid(),
          facebookId: req.body.id,
          displayName: req.body.name
        });
      }
      user.generateToken();
      await user.save();
      return res.send(user);
    } catch(e) {
      return res.send(e);
    }
  });

  return router;
};

module.exports = createRouter;
