const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const users = require("./app/users");
const places = require("./app/places")
const images = require("./app/images")
const reviews = require("./app/reviews")
const config = require("./config");
const app = express();
const PORT = process.env.NODE_ENV === "test" ? 8010 : 8000;

app.use(cors());
app.use(express.static("public"));
app.use(express.json());

mongoose.connect(`${config.db.url}/${config.db.name}`, {useNewUrlParser: true})
    .then(() => {
        console.log("Mongoose connected!");
        app.use("/users", users());
        app.use("/places", places());
        app.use("/images", images());
        app.use("/reviews", reviews());

        app.listen(PORT, () => {
            console.log("Server started at http://localhost:" + PORT);
        });
    });





