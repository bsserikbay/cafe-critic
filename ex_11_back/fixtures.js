const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");

mongoose.connect(`${config.db.url}/${config.db.name}`);

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("users");
    } catch (e) {
        console.log("Collection were not presented. Skipping drop...");
    }

    const [user, admin] = await User.create({
        username: "user",
        password: "user",
        role: "user"
    }, {
        username: "admin",
        password: "admin",
        role: "admin"
    });

    db.close();
});
